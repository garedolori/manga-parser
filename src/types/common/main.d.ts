interface MangaRemoteSearchResult {
    image: string
    label: string
    link: string
    updated: string
    value: string
    year: string
}

interface ActionRoute {
    name: string
    route: string
    handler: Function
}

interface ParsedRoute  {
    route: string;
    handler: Function;
    name:string;
}

interface RouteParams {
    [name:string]: string|number
}

interface ParsedRoutesMap {
    [name:string]: ParsedRoute
}

interface MenuButton {
    name: string;
    route: string;
}

interface Menu {
    message: string,
    buttons?: MenuButton[]
}

interface ChaptersImagesMap {
    [name:string]: string[]
}

interface VolumesChaptersImagesMap {
    [name:number]: ChaptersImagesMap
}

declare type GeneratorFileType = "mobi" | "epub";

declare type GeneratorReturn = {
    [key in GeneratorFileType]: string[]
}

type FileGeneratorMap = {
    [name in GeneratorFileType]: any;
};
