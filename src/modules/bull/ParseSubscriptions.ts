import {Telegraf, Markup} from "telegraf";
import {Queue, QueueScheduler, Worker} from "bullmq";
import IORedis from "ioredis";
import {Subscription} from "../../db/entities/Subscription";
import MangaRemote from "../../components/MangaRemote";
import {Manga} from "../../db/entities/Manga";
import {User} from "../../db/entities/User";
import {Volume} from "../../db/entities/Volume";
import {Chapter} from "../../db/entities/Chapter";
import App from "../../App";


export default class ParseSubscriptions {
    private queueName = 'notification';
    private queue:Queue;

    constructor(private bot:Telegraf) {
        const connection = new IORedis(process.env.REDIS_HOST);

        this.queue = new Queue(this.queueName, {connection});
        const repeatableQueue = new QueueScheduler(this.queueName, {connection});
        const worker = new Worker(this.queueName, job => this.runJob(job), {
            connection
        });

        worker.on("error", console.log);
    }

    async init() {
        await this.queue.removeRepeatableByKey('parse');

        await this.queue.add('parse', null, {
            repeat: {
                every: (3600 * 1000)
                // every: 60 * 1000
            }
        })
    }

    public async runJob(job) {
        console.log('[SCHEDULER] Running job');

        const subscriptions = await Subscription.find({relations: ['manga', 'user']});

        const mangaSubscribers:{[name:number]: User[]} = {};

        const mangaById:{[name:number]:Manga} = {};

        for (let {manga_id, manga, user} of subscriptions) {
            if (!mangaSubscribers[manga_id]) {
                mangaSubscribers[manga_id] = [];
            }

            mangaSubscribers[manga_id].push(user);

            if (!mangaById[manga_id]) {
                mangaById[manga_id] = manga
            }
        }

        for (let [manga_id, manga] of Object.entries(mangaById)) {
            const remoteManga = await MangaRemote.fromUrl(manga.root_url);

            if (remoteManga.chapters > manga.chapters_count) {
                const chapters = (await remoteManga.getChapterList()).reverse();
                const volumes = await remoteManga.getVolumeList();

                const newChaptersCount = remoteManga.chapters - manga.chapters_count;

                console.log(`[SCHEDULER] found ${newChaptersCount} new chapters of ${manga.name}`);

                manga.chapters_count = remoteManga.chapters;
                await manga.save({reload: true});

                const newChapters = chapters.slice(0, newChaptersCount);

                let newVolumeId;

                for (let newChapter of newChapters) {
                    let volume = await Volume.findOne({where: {name: newChapter.volume, manga_id: manga.id}});

                    if (!volume) {
                        volume = await Volume.create({
                            manga_id: parseInt(manga_id),
                            name: newChapter.volume,
                            num: volumes.length
                        }).save({reload:true});

                        newVolumeId = volume.id;
                    }

                    console.log(`[SCHEDULER] saving chapter ${newChapter.name} of ${manga.name} to db`);

                    const chapter = await Chapter.create({
                        manga_id: manga.id,
                        name: newChapter.name,
                        volume_id: volume.id,
                        root_url: newChapter.url,
                        number: chapters.length
                    }).save({reload: true});

                    await this.notifySubscribersChapter(chapter, manga, mangaSubscribers[manga_id]);
                }

                if (manga.is_ongoing && !remoteManga.isOngoing) {
                    console.log(`[SCHEDULER] manga ${manga.name} is finished`);
                    await this.notifySubscribersVolumeFinished(manga.id, mangaSubscribers[manga_id], true);
                    await this.notifySubscribersMangaFinished(manga, mangaSubscribers[manga_id]);
                    manga.is_ongoing = false;
                }

                await manga.save();

                if (newVolumeId) {
                    console.log(`[SCHEDULER] found new volume of ${manga.name}`)
                    await this.notifySubscribersVolumeFinished(manga.id, mangaSubscribers[manga_id]);
                }
            }
        }
    }

    private async notifySubscribersChapter(chapter:Chapter, manga:Manga, subscribers:User[]) {

        console.log(`[SCHEDULER] generating chapter files for ${chapter.name} of ${manga.name}`);

        for (let type of ["mobi", "epub"]) {
            const generator = await MangaRemote.getChapterGenerator(chapter.id, type as GeneratorFileType);
            const files = await generator.getFile();

            await chapter.setFiles(files, type as GeneratorFileType);
        }

        const keyboard = Markup.inlineKeyboard([
            [Markup.button.callback("Скачать EPUB",
                App.getInstance().router.to("chapterDownload", {chapter_id: chapter.id, type: 'epub'}
                )
            )],
            [Markup.button.callback("Скачать ZIP",
                App.getInstance().router.to("chapterDownload", {chapter_id: chapter.id, type: 'zip'}
                )
            )],
            [Markup.button.callback("📚 Манга",
                App.getInstance().router.to("manga", {manga_id: manga.id}
                )
            )]
        ]);

        console.log(`[SCHEDULER] sending notification about new chapter of ${manga.name} to ${subscribers.length} users`);

        await Promise.all(subscribers.map(user =>
            this.bot.telegram.sendMessage(user.key,
            `📓Вышла новая глава манги "${manga.name}" : \n\n ${chapter.name}`,
            keyboard)
        ));
    }

    private async notifySubscribersVolumeFinished(manga_id:number, subscribers:User[], mangaFinished:boolean = false) {
        const volumes = await Volume.find({
            where: {manga_id},
            order: {num: "DESC"},
            relations: ["manga", "chapters"]
        });

        const lastVolume = mangaFinished ? volumes[0] : volumes[1];

        for (let type of ["mobi", "epub"]) {
            const generator = await MangaRemote.getVolumeGenerator(lastVolume, type as GeneratorFileType);

            const files = await generator.getFile();

            await lastVolume.setFiles(files, type as GeneratorFileType);
        }

        const inlineKeyboard = Markup.inlineKeyboard([
            [Markup.button.callback('Скачать EPUB', App.getInstance().router.to("volumeDownload", {
                volume_id: lastVolume.id,
                type: "epub"
            }))],
            [Markup.button.callback('Скачать ZIP', App.getInstance().router.to("volumeDownload", {
                volume_id: lastVolume.id,
                type: "zip"
            }))],
            [Markup.button.callback('📚 Манга', App.getInstance().router.to("manga", {
                manga_id: lastVolume.manga_id,
            }))],
        ]);

        await Promise.all(subscribers.map(user =>
            this.bot.telegram.sendMessage(user.key,
                `У манги "${lastVolume.manga.name}" вышел полный том:\n\n${lastVolume.name}`,
                inlineKeyboard
                )
        ))
    }

    private async notifySubscribersMangaFinished(manga, subscribers:User[]) {
        for (let type of ["epub", "mobi"]) {
            const generator = await MangaRemote.geMangaGenerator(manga.id, type as GeneratorFileType);
            const files = await generator.getFile();
            await manga.setFiles(files, type as GeneratorFileType);
        }

        const buttons = Markup.inlineKeyboard([
            [Markup.button.callback('📚 Манга',App.getInstance().router.to('manga', {manga_id:manga.id}))],
            [Markup.button.callback('Скачать EPUB',App.getInstance().router.to('mangaFullDownload', {manga_id:manga.id, type: "epub"}))],
            [Markup.button.callback('Скачать ZIP',App.getInstance().router.to('mangaFullDownload', {manga_id:manga.id, type: "zip"}))],
            [Markup.button.callback('📓 Главы',App.getInstance().router.to('chapters', {manga_id:manga.id}))],
            [Markup.button.callback('📕 Томы',App.getInstance().router.to('volumes', {manga_id:manga.id}))],
            [Markup.button.callback('Отписаться',App.getInstance().router.to('unsubscribe', {manga_id:manga.id}))],
        ]);

        await Promise.all(subscribers.map(user =>
            this.bot.telegram.sendMessage(user.key, `Манга "${manga.name}" завершилась`, buttons)
        ))
    }
}