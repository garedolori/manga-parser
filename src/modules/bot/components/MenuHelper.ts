import {Context, Markup} from "telegraf";
import {User} from "../../../db/entities/User";
import {InputFile} from "telegraf/typings/core/types/typegram";

export default class MenuHelper {
    constructor(private context:Context) {
    }

    private _user:User;
    private async getUser() {
        if (!this._user) {
            this._user = await User.findOne({where: {key: this.context.chat.id}});
        }

        return this._user;
    }

    public async mainMenu() {
        await this.deleteMenuMessage();
        const message = 'Главное меню';

        const buttons = [
            [{name: '🔍 Поиск', route: 's/'}],
            [{name: '👤 Профиль', route: 'p/'}],
            [{name: '📚 Подписки', route: 'p/ss/'}],
            [{name: '🔖 Закладки', route: 'p/bs/'}],
        ]

        await this.setMenu(message, buttons);
    }

    public async setMenu(message:string, buttons:Array<any>= []) {
        const keyboard = Markup.inlineKeyboard(this.getButtons(buttons));

        const user = await this.getUser();

        if (!user.menu_message_id) {
            user.menu_message_id = await this.sendNewMenu(message, keyboard);
        } else {
            try {
                await this.updateMenu(message, keyboard);
            } catch (e) {
                if (user.menu_message_id) {
                    await this.deleteMenuMessage();
                }

                user.menu_message_id = await this.sendNewMenu(message, keyboard);
            }
        }
        await user.save();
    }

    public async setMenuImage(image:string|InputFile, message:string, buttons:Array<any>) {
        await this.deleteMenuMessage();

        const inlineKeyboard = Markup.inlineKeyboard(this.getButtons(buttons)).reply_markup;

        const user = await this.getUser();

        user.menu_message_id = (await this.context.replyWithPhoto(image, {
            caption: message,
            reply_markup: inlineKeyboard,
            disable_notification: true
        })).message_id;

        await user.save();
    }

    public async setMenuDocument(document:string, message:string, buttons:MenuButton[]|MenuButton[][]):Promise<void> {
        await this.deleteMenuMessage();

        const inlineKeyboard = Markup.inlineKeyboard(this.getButtons(buttons)).reply_markup;

        const user = await this.getUser();

        user.menu_message_id = (await this.context.replyWithDocument(document, {
            reply_markup: inlineKeyboard
        })).message_id;

        await user.save();
    }

    public async deleteMenuMessage() {
        const user = await this.getUser();

        if (!user.menu_message_id) {
            return;
        }
        try {
            await this.context.telegram.deleteMessage(this.context.chat.id, user.menu_message_id);
            user.menu_message_id = null;
            await user.save();
        } catch(e) {
            console.warn("unable to delete menu message")
        }
    }

    private async sendNewMenu(message, keyboard) {
        const menuMessage = await this.context.telegram.sendMessage(this.context.chat.id, message, {
            reply_markup: keyboard.reply_markup,
            disable_notification: true
        });

        return menuMessage.message_id;
    }

    private async updateMenu(message, keyboard) {
        const user = await this.getUser();
        return this.context.telegram.editMessageText(this.context.chat.id, user.menu_message_id, null, message, {
            reply_markup: keyboard.reply_markup,
        });
    }

    private getButtons(buttons) {
        const markupButtons = [];
        for (let button of buttons) {
            if (button instanceof Array) {
                markupButtons.push(this.getButtons(button))
            } else {
                markupButtons.push(Markup.button.callback(button.name, button.route));
            }
        }
        return markupButtons;
    }
}