import {Manga} from "../../../db/entities/Manga";
import MangaRemote from "../../../components/MangaRemote";
import {Chapter} from "../../../db/entities/Chapter";
import {Volume} from "../../../db/entities/Volume";
import {In} from "typeorm";

export default class DownloadableHelper {
    public fileTypeAttributeMap = {
        zip: "zip_file_url",
        epub: "file_url"
    }

    public async populateDb(manga_id:number):Promise<void> {
        const manga = await Manga.findOne({where: {id: manga_id}, relations: ['chapters', 'volumes']});

        if (manga.chapters.length && manga.volumes.length) {
            return;
        }

        const remoteManga = await MangaRemote.fromUrl(manga.root_url);

        const volumes = await remoteManga.getVolumeList();
        const chapters = await remoteManga.getChapterList();

        await Promise.all(chapters.map((chapter, index) =>
            Chapter.create({
                name: chapter['name'],
                number: ++index,
                manga_id: manga.id,
                root_url: chapter['url']
            }).save()
        ));

        for (let volume of volumes) {
            const volumeModel = await Volume.create({
                name: volume['name'],
                manga_id: manga.id,
                num: volume.num
            }).save({reload: true});

            await Chapter.update({
                root_url: In(volume['chapters'].map(chapter => chapter['url']))
            }, {volume_id: volumeModel.id});
        }
    }
}