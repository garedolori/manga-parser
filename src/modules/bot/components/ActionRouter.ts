import qs from "querystring";

export default class ActionRouter {
    private static instance?:ActionRouter;

    private routes:ParsedRoutesMap = {};

    public static getInstance():ActionRouter {
        if (!this.instance) {
            this.instance = new ActionRouter();
        }

        return this.instance;
    }

    public add(route:ActionRoute):void {
        this.routes[route.name] = {
            route: route.route,
            handler: route.handler,
            name: route.name
        };
    }

    public to(name:string, params?:{[name:string]:string|number}):string {
        const route = this.routes[name];

        let routeString = route.route;

        if (!route) {
            throw new Error(`Unknown route name: ${name}`);
        }

        const query = params ? qs.stringify(params) : ''

        return `${routeString}?${query}`;
    }

    public async run(context) {
        const route = context.callbackQuery.data;

        const currentRoute = this.getCurrentRoute(route);

        return currentRoute.handler(context, currentRoute);
    }

    protected getCurrentRoute(route:string):ParsedRoute {

        const [path, query] = route.split("?");

        for (let parsedRoute of Object.values(this.routes)) {
            if (parsedRoute.route === path) {
                const params = query ? qs.parse(query) : {};
                return Object.assign({}, parsedRoute, {params, originalUrl: route})
            }
        }

        throw new Error(`Unknown route: ${route}`);
    }
}