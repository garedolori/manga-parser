import {Context} from "telegraf";
import MenuHelper from "./MenuHelper";
import ActionRouter from "./ActionRouter";

export default class Controller {
    protected menuHelper:MenuHelper;
    protected router:ActionRouter;

    protected constructor(protected context:Context, protected route?) {
        this.menuHelper = new MenuHelper(this.context);
        this.router = ActionRouter.getInstance();
    }

    public static action(action:string):Function {
        const constructor = this;

        return (context:Context, route) => {
            const instance = new constructor(context, route);

            const actionToCall = instance[action];

            if (!actionToCall || !actionToCall.call) {
                throw new Error(`unknown action: ${action}`)
            }
            try {
                return actionToCall.bind(instance).call();
            } catch (e) {
                console.log(e.message);
            }
        }
    }
}