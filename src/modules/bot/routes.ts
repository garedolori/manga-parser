import ActionRouter from "./components/ActionRouter";
import ProfileController from "./controllers/ProfileController";
import AppController from "./controllers/AppController";
import MangaController from "./controllers/MangaController";
import ChapterController from "./controllers/ChapterController";
import VolumeController from "./controllers/VolumeController";

const router = ActionRouter.getInstance();

router.add({
    name: 'subscriptions',
    route: 'p/ss/',
    handler: ProfileController.action('subscriptions')
});

router.add({
    name: 'bookmarks',
    route: 'p/bs/',
    handler: ProfileController.action('subscriptions')
});

router.add({
    name: 'profile',
    route: 'p/',
    handler: ProfileController.action('index')
});

router.add({
    name: 'searchResults',
    route: 'sr/',
    handler: MangaController.action('search')
});

router.add({
    name: 'mangaDownloadOptions',
    route: 'm/do/:manga_id',
    handler: MangaController.action('downloadOptions')
});

router.add({
    name: 'mangaDownload',
    route: 'm/d/',
    handler: MangaController.action('downloadAll')
});

router.add({
    name: 'mangaFullDownload',
    route: 'm/fd/',
    handler: MangaController.action('downloadSingleBook')
});

router.add({
    name: 'manga',
    route: 'm/',
    handler: MangaController.action('manga')
});

router.add({
    name: 'volumes',
    route: 'vls/',
    handler: VolumeController.action('index')
})

router.add({
    name: 'volumeChapters',
    route: 'vl/chs/',
    handler: ChapterController.action('index')
})

router.add({
    name: 'volumeDownload',
    route: 'vl/d/',
    handler: VolumeController.action('download')
});

router.add({
    name: 'volume',
    route: 'vl/',
    handler: VolumeController.action('volume')
})


router.add({
    name: 'chapters',
    route: 'chs/',
    handler: ChapterController.action('index')
});

router.add({
    name: 'chapterDownload',
    route: 'ch/d/',
    handler: ChapterController.action('download')
})

router.add({
    name: 'chapter',
    route: 'ch/',
    handler: ChapterController.action('chapter')
})


router.add({
    name: 'subscribe',
    route: 'm/s/',
    handler: MangaController.action('subscribe')
});

router.add({
    name: 'unsubscribe',
    route: 'm/us/',
    handler: MangaController.action('unsubscribe')
})

router.add({
    name: 'addBookmark',
    route: 'm/b/',
    handler: MangaController.action('addBookmark')
});

router.add({
    name: 'removeBookmark',
    route: 'm/ub/',
    handler: MangaController.action('removeBookmark')
});

router.add({
    name: 'search',
    route: 's/',
    handler: AppController.action('searchQuery')
});

router.add({
    name: 'home',
    route: 'h/',
    handler: AppController.action('home')
});

export default router;