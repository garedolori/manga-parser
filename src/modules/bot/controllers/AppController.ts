import {Markup} from "telegraf";
import Controller from "../components/Controller";

export default class AppController extends Controller {

    public static SEARCH_QUESTION = 'Введите запрос:';

    public async home() {
        await this.menuHelper.mainMenu();
    }

    public async searchQuery() {
        await this.context.reply(AppController.SEARCH_QUESTION, Markup.forceReply());
        await this.menuHelper.deleteMenuMessage();
    }
}