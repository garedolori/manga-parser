import Controller from "../components/Controller";
import MangaRemote from "../../../components/MangaRemote";
import AppController from "./AppController";
import {Manga} from "../../../db/entities/Manga";
import {User} from "../../../db/entities/User";
import {Subscription} from "../../../db/entities/Subscription";
import DownloadableHelper from "../components/DownloadableHelper";
import {Bookmark} from "../../../db/entities/Bookmark";
import {Chapter} from "../../../db/entities/Chapter";

export default class MangaController extends Controller {
    private downloadHelper: DownloadableHelper;

    constructor(context, route) {
        super(context, route);

        this.downloadHelper = new DownloadableHelper();
    }

    public async search() {
        const {message} = this.context;
        let query: string;

        if (this.route && this.route.params.query) {
            query = this.route.params.query;
        } else {
            const reply = message['reply_to_message'];

            if (!reply || reply.text !== AppController.SEARCH_QUESTION) {
                return;
            }

            await this.context.telegram.deleteMessage(this.context.chat.id, message.message_id);
            await this.context.telegram.deleteMessage(this.context.chat.id, reply.message_id);

            query = message['text'];
        }

        if (!query) return;

        const searchResults = await MangaRemote.search(query);

        const buttons = searchResults.map(manga => [
            {
                name: manga.name, route: this.router.to('manga', {
                    manga_id: manga.id
                })
            }
        ]);

        buttons.push([
            {name: "🔍 Новый поиск", route: this.router.to('search')},
        ]);

        buttons.push([
            {name: '🏠 Домой', route: this.router.to('home')}
        ]);

        await this.menuHelper.setMenu(`Результаты по запросу: "${query}"`, buttons);
    }

    public async manga() {
        const {manga_id} = this.route.params;

        const manga = await Manga.findOne({where: {id: manga_id}, relations: ['chapters']});

        if (manga.chapters_count && !manga.chapters) {
            await this.downloadHelper.populateDb(manga_id);
        }

        const remoteManga = await MangaRemote.fromUrl(manga.root_url);

        if (!manga.chapters_count) {
            manga.chapters_count = remoteManga.chapters;
            await manga.save({reload: true});
        }

        if (manga.is_ongoing === null) {
            manga.is_ongoing = remoteManga.isOngoing;
            await manga.save({reload: true});
        }

        let buttons = [
            [{name: '🏠 Домой', route: this.router.to('home')}],
        ];

        if (remoteManga.chapters !== 0) {
            buttons = [
                [{name: '📓 Главы', route: this.router.to('chapters', {manga_id})}],
                [{name: '📕 Томы', route: this.router.to('volumes', {manga_id})}],
                [{name: '💾 Скачать все', route: this.router.to("mangaDownloadOptions", {manga_id})}],
            ].concat(buttons);
        }

        if (manga.is_ongoing) {
            const lastChapter = await Chapter.findOne({where: {manga_id}, order: {number: "DESC"}});

            if (lastChapter) {
                buttons.splice(-2, 0, [{
                    name: "Последняя вышедшая глава",
                    route: this.router.to("chapter", {chapter_id: lastChapter.id})
                }]);
            }
        } else {
            buttons.splice(-1, 0, [{
                name: '💾 Скачать все одной книгой',
                route: this.router.to("mangaDownloadOptions", {manga_id, singleBook: 1})
            }])
        }

        const user = await User.findOne({where: {key: this.context.from.id}});

        if (manga.is_ongoing) {
            const subscribed = await Subscription.findOne({where: {manga_id, user_id: user.id}});

            const subMenuEntry = subscribed
                ? [{"name": "Отписаться", route: this.router.to("unsubscribe", {manga_id})}]
                : [{"name": "Подписаться", route: this.router.to("subscribe", {manga_id})}]

            buttons.splice(2, 0, subMenuEntry)
        }

        const bookmark = await Bookmark.findOne({where: {user_id: user.id, manga_id}});

        const bookmarkMenuEntry = bookmark
            ? [{"name": "Убрать из закладок", route: this.router.to("removeBookmark", {manga_id})}]
            : [{"name": "В закладки", route: this.router.to("addBookmark", {manga_id})}];

        buttons.splice(2, 0, bookmarkMenuEntry);

        let message = `${remoteManga.name} \nстатус: ${remoteManga.isOngoing ? 'выходит' : 'заврешена'}\nглавы: ${remoteManga.chapters || "нет"} \n\n ${remoteManga.description}`;

        if (message.length > 1024) {
            message = `${message.slice(0, 1021)}...`;
        }

        await this.menuHelper.setMenuImage({url: remoteManga.coverUrl}, message, buttons);
    }

    public async subscribe() {
        const user = await User.findOne({where: {key: this.context.from.id}});
        await user.subscribe(this.route.params.manga_id);
        await this.context.answerCbQuery("Вы подписаны");

        await this.manga();

        await this.downloadHelper.populateDb(this.route.params.manga_id);
    }

    public async unsubscribe() {
        const user = await User.findOne({where: {key: this.context.from.id}});
        await user.unsubscribe(this.route.params.manga_id);
        await this.context.answerCbQuery("ВЫ отписались");

        await this.manga();
    }

    public async addBookmark() {
        const user = await User.findOne({where: {key: this.context.from.id}});
        await user.addToBookmarks(this.route.params.manga_id);
        await this.context.answerCbQuery("Закладка добавлена");

        await this.manga();

        await this.downloadHelper.populateDb(this.route.params.manga_id);
    }

    public async removeBookmark() {
        const user = await User.findOne({where: {key: this.context.from.id}});
        await user.removeBookmark(this.route.params.manga_id);
        await this.context.answerCbQuery("Закладка удалена");

        await this.manga();
    }

    public async downloadOptions() {
        const {manga_id} = this.route.params;
        await this.downloadHelper.populateDb(manga_id);

        const manga = await Manga.findOne({where: {id: manga_id}, relations: ["volumes", "chapters"]});

        const endpoint = !this.route.params.singleBook ? 'mangaDownload' : 'mangaFullDownload';

        return this.menuHelper.setMenu(`${manga.name}
        \nТомов:${manga.volumes.length}\nГлав:${manga.chapters.length}\nСтатус: ${manga.is_ongoing ? "выходит" : "завершена"}
        \nПосле выбора формата, начнется процесс загрузки фалов. Он может быть мгновенным, а может занять до 15 минут, в зависимости от того, были ли раньше запрошены файлы этой манги.`, [
            [
                {name: "Скачать EPUB", route: this.router.to(endpoint, {manga_id, type: "epub"})}
            ],
            [
                {name: "Скачать MOBI", route: this.router.to(endpoint, {manga_id, type: "mobi"})}
            ],
            [
                {name: "📚 Назад к манге", route: this.router.to("manga", {manga_id})}
            ],
            [
                {name: "🏠 Домой", route: this.router.to("home")}
            ],
        ])
    }

    public async downloadAll() {
        const {manga_id, type} = this.route.params;

        const manga = await Manga.findOne({
            where: {
                id: manga_id
            },
            relations: ["volumes", "volumes.chapters", "volumes.manga"],
        });

        await this.context.telegram.sendMessage(this.context.from.id, "Подготовка. Пожалуйста подождите. Некоторые файлы могут генерироваться с задержкой.\n\nПо окончанию процесса, вы увидете сообщение об этом")

        const orderedVolumes = manga.volumes.sort((previous, next) =>
            previous.num < next.num ? -1 : 1
        );

        const volumeNums: number[] = orderedVolumes.map(volume => volume.num);

        const currentVolume = manga.is_ongoing ? Math.max.apply(null, volumeNums) : 0;

        new Promise(async (resolve, reject) => {
            for (let volume of orderedVolumes) {
                if (volume.num !== currentVolume) {
                    await volume.getFiles(type)
                        .then(files => {
                            return files.length
                                ? files.map(file => file.file_id)
                                : MangaRemote.getVolumeGenerator(volume, type)
                                    .then(generator =>
                                        this.menuHelper.deleteMenuMessage()
                                            .then(() => this.menuHelper.setMenu(`Генерируется файл ${volume.name}...`))
                                            .then(() => generator.getFile())
                                            .then(file_ids => volume.setFiles(file_ids, type)
                                                .then(() => file_ids)
                                            )
                                    )
                        })
                        .then(fileIds => Promise.all(
                            fileIds.map(file_id =>
                                this.context.telegram.sendDocument(this.context.chat.id, file_id)
                            )
                        )).catch(reject)
                } else {
                    const orderedChapters = volume.chapters.sort((a, b) =>
                        a.number < b.number ? -1 : 1
                    );

                    for (let chapter of orderedChapters) {
                        await chapter.getFiles(type)
                            .then(files => {
                                return files.length
                                    ? files.map(file => file.file_id)
                                    : MangaRemote.getChapterGenerator(chapter.id, type)
                                        .then(generator =>
                                            this.menuHelper.deleteMenuMessage()
                                                .then(() => this.menuHelper.setMenu(`Генерируется файл ${chapter.name}...`))
                                                .then(() => generator.getFile())
                                                .then(file_ids =>
                                                    chapter.setFiles(file_ids, type)
                                                        .then(() => file_ids)
                                                )
                                        )
                            })
                            .then(fileIds => Promise.all(
                                fileIds.map(file_id =>
                                    this.context.telegram.sendDocument(this.context.chat.id, file_id)
                                )
                            )).catch(reject)
                    }
                }
            }
            return resolve(true);
        })
            .then(() => this.menuHelper.deleteMenuMessage())
            .then(() =>
                this.menuHelper.setMenu(`Все доступные файлы манги "${manga.name}" были высланы.`, [
                    [{name: "📚 Назад к манге", route: this.router.to("manga", {manga_id})}],
                    [{name: "🏠 Домой", route: this.router.to("home")}]
                ]))
            .catch((e) => {
                    console.log(e);
                    return this.context.telegram.sendMessage(this.context.from.id, "Что-то пошло не так. Попробуйте еще раз позже.")
                        .then(() => this.downloadOptions())
                }
            )
    }

    public async downloadSingleBook() {
        const {manga_id, type} = this.route.params;

        const manga = await Manga.findOne(manga_id);

        const files = await manga.getFiles(type);

        if (!files.length) {
            await this.menuHelper.deleteMenuMessage();
            await this.menuHelper.setMenu("Подготовка. Пожалуйста подождите.\nПроцесс генерации книги может занять некоторое время", []);

            const generator = await MangaRemote.geMangaGenerator(manga_id, type);

            const notificationTextTemplate = `Загрузка ${manga.name}:\nЗагружется том {currentVolume} из {totalVolumes}`

            generator.on('volumeDownload', message => {
                const notificationText = notificationTextTemplate
                    .replace('{currentVolume}', `${message.current}`)
                    .replace('{totalVolumes}', `${message.total}`)
                this.menuHelper.setMenu(notificationText, []);
            });

            generator.on("downloadComplete", () => this.menuHelper.setMenu("Сшиваем файл...", []));

            generator.on("filesizeExceeded", part => this.menuHelper.setMenu(`Размер файла превысил 1.5 Гб. 
            \nТелеграм не позволяет отправлять файлы более 2 Гб, потому будет сгенерирована ${part + 1} часть..`));

            if (generator.isProcessRunning) {
                generator.on("finish", file_ids => this.sendFullManga(file_ids, manga));
            } else {
                new Promise((resolve, reject) =>
                    generator.getFile()
                        .then(file_ids =>
                            this.menuHelper.setMenu('Загружаем файл...', [])
                                .then(() => {
                                    manga.setFiles(file_ids, type)
                                        .then(() => this.sendFullManga(file_ids, manga))
                                        .then(() => generator.removeFile())
                                })
                        )
                        .then(resolve)
                        .catch(reject)
                )
                    .catch(e => {
                        console.error(e);
                        this.context.telegram.sendMessage(this.context.chat.id, 'Что-то пошло не так, попробуйте снова чуть позже.');
                    })
            }
        } else {
            await this.sendFullManga(files.map(file => file.file_id), manga)
        }
    }

    private sendFullManga(files, manga) {
        return Promise.all(
            files.map(file =>
                this.context.telegram.sendDocument(this.context.chat.id, file)
            ).concat([
                this.menuHelper.deleteMenuMessage()
                    .then(() =>
                        this.menuHelper.setMenu(`Манга "${manga.name}" была выслана`, [
                            [{name: "📚 Назад к манге", route: this.router.to("manga", {manga_id: manga.id})}],
                            [{name: "🏠 Домой", route: this.router.to("home")}]
                        ])
                    )
            ])
        );
    }
}