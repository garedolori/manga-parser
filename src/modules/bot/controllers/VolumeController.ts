import Controller from "../components/Controller";
import {Manga} from "../../../db/entities/Manga";
import {Volume} from "../../../db/entities/Volume";
import MangaRemote from "../../../components/MangaRemote";
import App from "../../../App";
import DownloadableHelper from "../components/DownloadableHelper";
import {In} from "typeorm";

export default class VolumeController extends Controller {
    private downloadableHelper: DownloadableHelper;

    constructor(context, route) {
        super(context, route);
        this.downloadableHelper = new DownloadableHelper();
    }

    public async index() {
        const sort = this.route.params.sort || "DESC";

        const {manga_id} = this.route.params;

        await this.downloadableHelper.populateDb(manga_id);

        const page = this.route.params.page ? parseInt(this.route.params.page) : 1;

        const manga = await Manga.findOne({where: {id: manga_id}});

        const volumesCount = await Volume.count({where: {manga_id}});

        const volumes = await Volume.find({
            where: {manga_id},
            order: {"num": sort},
            take: 20,
            skip: (page - 1) * 20
        });

        const maxVolume = await Volume.findOne({
            where: {manga_id},
            select: ["id", "num"],
            order: {num: "DESC"}
        });

        const buttons = volumes.map(volume => [
            {
                name: `${volume.name} ${manga.is_ongoing && volume.id === maxVolume.id ? '(выходит)' : ''}`,
                route: this.router.to('volume', {volume_id: volume.id})
            }
        ]);

        if (volumesCount > 20) {
            const pagesCount = Math.ceil(volumesCount / 20);
            const nav: MenuButton[] = [{name: `${page}/${pagesCount}`, route: 'nowhere'}];

            if (page !== 1)
                nav.unshift({
                    name: `⬅`,
                    route: this.router.to('volumes', {manga_id, page: page - 1})
                });
            if (page !== pagesCount)
                nav.push({
                    name: '➡️',
                    route: this.router.to('volumes', {manga_id, page: page + 1})
                });

            buttons.push(nav);
        }

        buttons.push([{
            name: sort === "DESC" ? "↕️ Сначачала старые" : "↕️ Сначала новые",
            route: this.router.to("volumes", Object.assign({}, this.route.params, {
                sort: sort === "ASC" ? "DESC" : "ASC",
                page: 1
            }))
        }])

        buttons.push([{name: '◀ Назад', route: this.router.to('manga', {manga_id})}]);

        await this.menuHelper.setMenu(`${manga.name} Томы:`, buttons);
    }

    public async volume() {
        const {volume_id} = this.route.params;

        const volume = await Volume.findOne({where: {id: volume_id}, relations: ['manga', 'chapters']});

        const maxVolume = await Volume.findOne({
            where: {manga_id: volume.manga_id},
            order: {num: "DESC"}
        });

        const siblings = await Volume.find({
            where: {
                num: In([volume.num + 1, volume.num - 1]),
                manga_id: volume.manga_id
            }
        });

        const isCurrent = volume.manga.is_ongoing
            ? volume.id === maxVolume.id
            : false;

        const buttons: MenuButton[] | Array<MenuButton[]> = [
            [{name: '📓 Главы', route: this.router.to('volumeChapters', {volume_id})}],
            [{name: '◀ Назад', route: this.router.to('volumes', {manga_id: volume.manga_id})}],
            [{name: '🏠 Домой', route: this.router.to('home')}]
        ];

        if (siblings.length) {
            const nav = [];

            const previous = siblings.find(sibling => sibling.num === volume.num - 1)
            if (previous) {
                nav.push({
                    name: `⬅ ${previous.name}`,
                    route: this.router.to("volume", {volume_id: previous.id})
                });
            }

            const next = siblings.find(sibling => sibling.num === volume.num + 1);
            if (next) {
                nav.push({
                    name: `${next.name} ➡️`,
                    route: this.router.to('volume', {volume_id: next.id})
                })
            }

            buttons.splice(1, 0, nav);
        }

        if (!isCurrent) {
            buttons.unshift([{
                name: 'Скачать MOBI',
                route: this.router.to('volumeDownload', {volume_id, type: "mobi"})
            }]);
            buttons.unshift([{
                name: 'Скачать Epub',
                route: this.router.to('volumeDownload', {volume_id, type: "epub"})
            }]);
        }
        const message = `${volume.manga.name}: ${volume.name} ${isCurrent ? '(выходит)' : ''}
        глав: ${volume.chapters.length}`;

        await this.menuHelper.setMenu(message, buttons)
    }

    public async download() {
        const {volume_id, type} = this.route.params;

        const volume = await Volume.findOne({where: {id: volume_id}, relations: ['manga', 'chapters']});

        const files = await volume.getFiles(type);

        if (!files.length) {
            await this.context.answerCbQuery('generating file..');
            await this.menuHelper.setMenu('Файл генерируется. Пожалуйста, подождите ⌛️');

            const generator = await MangaRemote.getVolumeGenerator(volume, type);

            const template = `Загрузка ${volume.name}:\nЗагружется глава {current} из {total}`

            generator.on("chapterDownload", data => {
                let message = template;
                for (let [key, value] of Object.entries(data)) {
                    message = message.replace(`{${key}}`, value.toString());
                }

                this.menuHelper.setMenu(message);
            })

            if (generator.isProcessRunning) {
                generator.on("finish", file_ids => this.sendVolumeMessage(volume, file_ids));

                return;
            }

            new Promise((resolve, reject) =>
                generator.getFile()
                    .then(file_ids =>
                        volume.setFiles(file_ids, type)
                            .then(() => this.sendVolumeMessage(volume, file_ids))
                    )
                    .then(resolve)
                    .catch(reject)
            )
        } else {
            await this.sendVolumeMessage(volume, files.map(file => file.file_id));
        }
    }

    private async sendVolumeMessage(volume: Volume, file_ids) {
        return Promise.all(
            file_ids.map(file_id =>
                this.context.telegram.sendDocument(this.context.chat.id, file_id)
            ).concat([
                this.menuHelper.setMenu(`${volume.manga.name} ${volume.name}`, [
                    [{name: "◀ Назад", route: this.router.to('volume', {volume_id: volume.id})}],
                    [{name: "⏪ Список томов", route: this.router.to('volumes', {manga_id: volume.manga_id})}],
                    [{name: "📚 Назад к манге", route: this.router.to('manga', {manga_id: volume.manga_id})}],
                    [{name: "🏠 Домой", route: this.router.to('home')}],
                ])
            ])
        );
    }
}