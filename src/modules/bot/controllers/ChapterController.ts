import Controller from "../components/Controller";
import DownloadableHelper from "../components/DownloadableHelper";
import {Manga} from "../../../db/entities/Manga";
import {Chapter} from "../../../db/entities/Chapter";
import MangaRemote from "../../../components/MangaRemote";
import {Volume} from "../../../db/entities/Volume";
import App from "../../../App";
import {In} from "typeorm";

export default class ChapterController extends Controller {
    private downloadableHelper:DownloadableHelper;

    constructor(context, route) {
        super(context, route);
        this.downloadableHelper = new DownloadableHelper();
    }

    public async index() {
        const sort = this.route.params.sort || "DESC";

        let {manga_id, volume_id} = this.route.params,
        where,
        title = '';

        if (volume_id) {
            where = { volume_id };
            const volume = await Volume.findOne({where: {id: volume_id}, relations: ['manga']});
            manga_id = volume.manga_id;
            title = `"${volume.manga.name}": ${volume.name} \n\nГлавы:`
        } else {
            where = {manga_id};
            const {name} = await Manga.findOne({where: {id: manga_id}});
            title = `${name} \n\n Главы:`
        }

        await this.downloadableHelper.populateDb(manga_id);

        const page = this.route.params.page ? parseInt(this.route.params.page) : 1;

        const chaptersCount = await Chapter.count({where});

        const chapters = await Chapter.find({
            where,
            order: { number: sort },
            take: 20,
            skip: (page -1) * 20
        });

        const buttons = chapters.map(chapter => [
            {name: chapter.name, route: this.router.to('chapter', {chapter_id: chapter.id})}
        ]);

        if (chaptersCount > 20) {
            const totalPages = Math.ceil(chaptersCount / 20 )

            const nav:MenuButton[] = [{name: `${page}/${totalPages}`, route: 'nowhere'}];

            if (page !== 1) {
                nav.unshift({name: '⬅', route: this.router.to('chapters', {manga_id, page: page-1})});
            }

            if (page !== totalPages) {
                nav.push({name: '➡', route: this.router.to('chapters', {manga_id, page: page+1})});
            }

            buttons.push(nav);
        }

        buttons.push([{
            name: sort === "DESC" ?  "↕️ Сначачала старые":"↕️ Сначала новые",
            route: this.router.to("chapters", Object.assign({}, this.route.params, {
                sort: sort === "ASC" ? "DESC" : "ASC",
                page: 1
            }))
        }])

        if (volume_id) {
            buttons.push([{name: "📓 Том", route: this.router.to('volume', {volume_id})}])
        }

        buttons.push([{name: '📚 Манга', route: this.router.to('manga', {manga_id})}]);
        buttons.push([{name: '🏠 Домой', route: this.router.to('home')}]);

        await this.menuHelper.setMenu(title, buttons);
    }

    public async download() {
        const {chapter_id, type} = this.route.params;

        const chapter = await Chapter.findOne({where: {id: chapter_id}, relations: ['manga']});

        const files = await chapter.getFiles(type);

        if (!files.length) {
            await this.context.answerCbQuery('generating file..');
            await this.menuHelper.setMenu('Файл генерируется. Пожалуйста подождите ⌛️');

            MangaRemote.getChapterGenerator(chapter_id, type)
                .then(generator => generator.getFile()
                        .then(file_id => chapter.setFiles(file_id, type)
                            .then(() => this.sendChapterMessage(chapter, file_id))
                        )
                );
        } else {
            await this.sendChapterMessage(chapter, files.map(file => file.file_id));
        }
    }

    private async sendChapterMessage(chapter:Chapter, file_ids:string[]):Promise<any> {
        await this.menuHelper.deleteMenuMessage();

        for (let file of file_ids) {
            await this.context.telegram.sendDocument(this.context.chat.id, file);
        }

        return this.menuHelper.setMenu(`Файл главы ${chapter.name} был выслан`, [
            [{name: "◀ Назад", route: this.router.to('chapter', {chapter_id: chapter.id})}],
            [{name: "️️⏪ Список глав", route: this.router.to('chapters', {manga_id: chapter.manga_id})}],
            [{name: "📕 Том", route: this.router.to('volume', {volume_id: chapter.volume_id})}],
            [{name: "📚 Манга", route: this.router.to('manga', {manga_id: chapter.manga_id})}],
            [{name: "🏠 Домой", route: this.router.to('home')}],
        ])
    }

    public async chapter() {
        const {chapter_id} = this.route.params;
        const chapter = await Chapter.findOne({where: {id: chapter_id}, relations: ['manga']});

        const buttons = [
            [{name: 'Скачать MOBI', route: this.router.to('chapterDownload', {chapter_id, type: "mobi"})}],
            [{name: 'Скачать EPUB', route: this.router.to('chapterDownload', {chapter_id, type: "epub"})}],
            [{name: '📓 Все главы', route: this.router.to('chapters', {manga_id: chapter.manga_id})}],
            [{name: "📕 Том", route: this.router.to('volume', {volume_id: chapter.volume_id})}],
            [{name: "📚 Манга", route: this.router.to('manga', {manga_id: chapter.manga_id})}],
            [{name: "🏠 Домой", route: this.router.to('home')}]
        ];

        const siblings = await Chapter.find({
            where: {
                manga_id: chapter.manga_id,
                number: In([chapter.number +1, chapter.number -1])
            },
        });

        if (siblings.length) {
            const nav = [];

            const previous = siblings.find(sibling => sibling.number === chapter.number - 1);
            if (previous) {
                nav.push({
                    name: `⬅ ${previous.name}`,
                    route: this.router.to('chapter', {chapter_id: previous.id})
                });
            }

            const next = siblings.find(sibling => sibling.number === chapter.number + 1);
            if (next) {
                nav.push({
                    name: `${next.name} ➡`,
                    route: this.router.to('chapter', {chapter_id: next.id})
                });
            }

            buttons.splice(2, 0, nav);
        }

        await this.menuHelper.setMenu(`${chapter.manga.name}: ${chapter.name}`, buttons);
    }
}