import Controller from "../components/Controller";
import moment from "moment";
import {User} from "../../../db/entities/User";

export default class ProfileController extends Controller {
    public async index() {
        const profile = await User.findOne({where: {key: this.context.from.id}, relations: ["manga"]});

        const memberFrom = moment(profile.created_at).format('m-d-Y');
        const message = `${profile.name} \nС нами с ${memberFrom} \nПодписки: ${profile.manga.length}`;

        await this.menuHelper.setMenu(message, [{name: '🏠 Домой', route: this.router.to('home')}]);
    }

    public async subscriptions() {
        const relationName = this.route.name === 'subscriptions' ? 'manga' : 'bookmarks'

        const user = await User.findOne({where: {key: this.context.from.id}, relations: [relationName]});

        const buttons = user[relationName].map(manga => ([{
            name: manga.name,
            route: this.router.to('manga', {manga_id: manga.id})
        }]));

        buttons.push([{
            name: '🏠 Домой',
            route: this.router.to('home')
        }]);

        const title = this.route.name === 'subscriptions' ? "Ваши подписки:" : "Ваши закладки:"

        await this.menuHelper.setMenu(title, buttons);
    }
}