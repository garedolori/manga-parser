import App from "./App";

const app = App.getInstance();

app.init()
    .then(() => app.run())
    .catch(e => console.error(e))