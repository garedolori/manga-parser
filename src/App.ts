import {Telegraf, Context, Markup} from "telegraf";
import {Connection, createConnection} from "typeorm";
import {TDLib} from "tdl-tdlib-addon";
import Client from "tdl";
import {User} from "./db/entities/User";
import MenuHelper from "./modules/bot/components/MenuHelper";
import TgFileUploader from "./components/TgFileUploader";
import MangaController from "./modules/bot/controllers/MangaController";
import ActionRouter from "./modules/bot/components/ActionRouter";
import router from "./modules/bot/routes";
import ParseSubscriptions from "./modules/bull/ParseSubscriptions";
import {Changelog} from "./db/entities/Changelog";
import moment from "moment";
import child_process from "child_process";

export default class App {
    public bot: Telegraf;
    public uploader: TgFileUploader;
    public router:ActionRouter;
    private connection: Connection;
    private apiClient:Client;
    private parser:ParseSubscriptions;

    private commands: object = {
        start: this.start,
        menu: this.drawMenu,
        test: this.test,
        'selfupdate': this.selfUpdate
    };

    private static instance:App;

    public static getInstance():App {
        if (!this.instance) {
            this.instance = new this();
        }

        return this.instance;
    }

    constructor() {
        this.bot = new Telegraf(process.env.BOT_TOKEN);
        this.router = router;
        this.parser = new ParseSubscriptions(this.bot);
    }

    public async getApiClient() {
        if (!this.apiClient) {
            const client = new Client(new TDLib('/td/tdlib/lib/libtdjson.so'), {
                apiId: parseInt(process.env.TDL_API_ID),
                apiHash: process.env.TDL_API_HASH
            });

            await client.connect();

            await client.login(() => ({
                getPhoneNumber: retry => retry
                    ? Promise.reject("Invalid phone")
                    : Promise.resolve(process.env.TDL_CLIENT_PHONE),
                getAuthCode: retry => retry
                    ? Promise.reject("Invalid phone")
                    : Promise.resolve('21601'),
            }));

            this.apiClient = client;
        }

        return this.apiClient;
    }

    public async init(): Promise<void> {
        await this.getApiClient();
        this.uploader = new TgFileUploader(await this.getApiClient());

        this.connection = await createConnection();

        for (let [command, handler] of Object.entries(this.commands)) {
            this.bot.command(command, handler.bind(this));
        }

        this.bot.action(/.*/, context => router.run(context).catch(e => {
            console.error(e.message);
            context.answerCbQuery("Что-то пошло не так >.<\" \nЕсли ошибка повторяется регулярно, то это плохо", {show_alert: true})
        }));

        this.bot.on('text', (context) => MangaController.action('search')(context));
        await this.parser.init();
        await this.notifyChanges();
    }

    public async run() {
        return Promise.all([
            this.bot.launch().catch(console.log)
        ])
    }

    private async notifyChanges() {
        const changelog = await Changelog.find({
            where: {
                is_sent: false
            },
            order: {
                created: "DESC"
            }
        });

        if (changelog.length) {
            const users = await User.find();

            for (let record of changelog) {
                record.is_sent = true;
                await record.save();

                await Promise.all(users.map(user =>
                    this.bot.telegram.sendMessage(user.key, `Новое обновление!\n\nВерсия: ${record.version}\n${moment(record.created).format("DD.MM.YYYY")}\nИзменения:\n${record.text}\n\nПриятного чтения ^__^`)
                ));
            }
        }
    }

    private async start(context: Context): Promise<void> {
        if (context.from?.is_bot === true) {
            return;
        }

        const chatId = context.chat?.id as number;
        const username = context.from?.username as string;

        const existingUser = await User.findOne({where: {key: chatId}});

        if (!existingUser) {
            const user = new User();
            user.name = username || "Anonimous";
            user.key = chatId.toString();
            user.is_premium = true;

            await user.save();
        }
            await context.reply(`Добро пожаловать в MangaBot!\n\nЭтот бот помогает искать и скачивать мангу с сайта mangapoisk.ru в формате электронных книг (epub и zip), а так же уведомляет о выходе новых глав и томов\nВ будущем, возможно, будут и другие источники и форматы =)\nЧтоб вызвать главное меню, используйте команду /menu\n\nПриятного чтения ^__^`);
    }

    public async drawMenu(context:Context): Promise<void> {
        const menuHelper = new MenuHelper(context);
        await menuHelper.mainMenu();
    }

    public async test(context:Context): Promise<any> {
        // await this.parser.runJob(null);

        /*const client = await this.getApiClient();

        try {
            await client.invoke({
                _:"getChats",
                'offset_order': '9223372036854775807',
                'offset_chat_id': 0,
                'limit': 100
            });

            const chats = await client.invoke({
                _:"getChats",
                'offset_order': '9223372036854775807',
                'offset_chat_id': 0,
                'limit': 100
            });

            for(let chat of chats.chat_ids) {
                console.log(await client.invoke({
                    _:"getChat",
                    chat_id: chat
                }));
            }
        } catch (e) {console.log(e)}*/
    }

    private async selfUpdate(context:Context) {
        if (context.from.id !== parseInt(process.env.ADMIN_USER_ID)) {
            return;
        }

        child_process.exec('git pull && shutdown -r now', {
            cwd: '/var/www'
        }, (error, stdout) => {
            if (error) {
                context.reply(`ERROR: ${error.message}`)
            }

            context.reply(`SUCCESS> ${stdout}`);
        });
    }

}
