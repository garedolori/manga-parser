import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class ChapterFileUrl1623152540283 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('chapter', new TableColumn({
            name: 'file_url',
            type: 'varchar',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('chapter', 'file_url');
    }

}
