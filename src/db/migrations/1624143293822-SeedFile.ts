import {MigrationInterface, QueryRunner} from "typeorm";
import {File} from "../entities/File";

export class SeedFile1624143293822 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        for (let essence of ["manga", "volume", "chapter"]) {
            const models = await queryRunner
                .query(`SELECT id, file_url, zip_file_url from ${essence} where file_url is not NULL or zip_file_url is not NULL`);

            for (let model of models) {
                if (model.file_url) {
                    await File.create({essence: "manga", essence_id: model.id, file_id: model.file_url, type: "epub"}).save();
                }

                if (model.zip_file_url) {
                    await File.create({essence: "manga", essence_id: model.id, file_id: model.zip_file_url, type: "zip"}).save();
                }
            }
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("delete from file where id");
    }

}
