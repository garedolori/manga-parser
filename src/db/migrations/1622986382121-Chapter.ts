import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class Chapter1622986382121 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'chapter',
            columns: [
                {
                    name: 'id',
                    type: 'int(11) unsigned auto_increment',
                    isPrimary: true,
                },
                {
                    name: 'manga_id',
                    type: 'int (11) unsigned'
                },
                {
                    name: 'name',
                    type: 'varchar'
                },
                {
                    name: 'number',
                    type: 'int(11) unsigned'
                },
                {
                    name: 'created_at',
                    type: 'datetime',
                    default: 'NOW()'
                }
            ]
        }));

        await queryRunner.createForeignKey('chapter', new TableForeignKey({
            columnNames: ['manga_id'],
            onDelete: "cascade",
            referencedColumnNames: ['id'],
            referencedTableName: "manga"
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('chapter')
    }

}
