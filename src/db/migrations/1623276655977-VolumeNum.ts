import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class VolumeNum1623276655977 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('volume', new TableColumn({name: "num", type: "int", isNullable: true}));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('volume', 'num');
    }

}
