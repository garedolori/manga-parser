import {MigrationInterface, QueryRunner} from "typeorm";
import {Changelog} from "../entities/Changelog";

export class ChangelogV1021623618875808 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await Changelog.create({
            is_sent: false,
            text: ` - Добавлена возможность запросить все файлы манги, вне зависимости от того, выходит она или уже вышла. Для онгоинг тайтлов будут высланы все вышедшие томы и все главы текущего тома.
            \n - Исправлена ошибка удаления манги из закладок`,
            version: '1.0.2'
        }).save()
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await Changelog.delete({version:'1.0.2'});
    }

}
