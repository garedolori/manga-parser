import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class File1624143059376 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "file",
            columns: [
                {
                    name: "id",
                    type: "int(11) unsigned auto_increment",
                    isPrimary: true
                },
                {
                    name: "essence",
                    type: "varchar"
                },
                {
                    name: "essence_id",
                    type: "int(11) unsigned"
                },
                {
                    name: "type",
                    type: "varchar"
                },
                {
                    name: "file_id",
                    type: "varchar"
                }
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('file');
    }

}
