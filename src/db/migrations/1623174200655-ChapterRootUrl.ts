import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class ChapterRootUrl1623174200655 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('chapter', new TableColumn({
            name: 'root_url',
            type: 'varchar'
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('chapter', 'root_url');
    }

}
