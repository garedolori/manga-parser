import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class Bookmark1623593522249 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'bookmark',
            columns: [
                {
                    name: 'id',
                    type: 'int(11) unsigned auto_increment',
                    isPrimary: true
                },
                {
                    name: 'manga_id',
                    type: 'int(11) unsigned'
                },
                {
                    name: 'user_id',
                    type: 'int(11) unsigned'
                }
            ]
        }));

        await queryRunner.createForeignKey('bookmark', new TableForeignKey({
            columnNames: ['manga_id'],
            onDelete: "cascade",
            referencedColumnNames: ['id'],
            referencedTableName: "manga"
        }))
        await queryRunner.createForeignKey('bookmark', new TableForeignKey({
            columnNames: ['user_id'],
            onDelete: "cascade",
            referencedColumnNames: ['id'],
            referencedTableName: "user"
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('bookmark')
    }

}
