import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class ChangelogVersion1623596664909 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('changelog', new TableColumn({name: 'version', type: 'varchar'}));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('changelog', 'version');
    }

}
