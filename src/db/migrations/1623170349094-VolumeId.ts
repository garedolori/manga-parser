import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class VolumeId1623170349094 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('chapter', new TableColumn({
            name: 'volume_id',
            type: 'int(11) unsigned',
            isNullable: true
        }));

        await queryRunner.createForeignKey('chapter', new TableForeignKey({
            columnNames: ['volume_id'], onDelete: "cascade", referencedColumnNames: ['id'], referencedTableName: "volume"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('chapter', 'volume_id');
    }

}
