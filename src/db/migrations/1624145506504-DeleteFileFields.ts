import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class DeleteFileFields1624145506504 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        for (let table of ["manga", "volume", "chapter"]) {
            await queryRunner.dropColumn(table, "file_url");
            await queryRunner.dropColumn(table, "zip_file_url");
        }
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        for (let table of ["manga", "volume", "chapter"]) {
            await queryRunner.addColumn(table, new TableColumn({name: "file_url", type: "varchar", isNullable: true}));
            await queryRunner.addColumn(table, new TableColumn({name: "zip_file_url", type: "varchar", isNullable: true}));
        }
    }

}
