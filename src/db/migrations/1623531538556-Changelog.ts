import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class Changelog1623531538556 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "changelog",
            columns: [
                {
                    name: "id",
                    type: "int(11) unsigned auto_increment",
                    isPrimary: true
                },
                {
                    name: "is_sent",
                    type: "boolean"
                },
                {
                    name: "text",
                    type: "text"
                },
                {
                    name: "created",
                    type: "datetime",
                    default: 'NOW()'
                }
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("changelog");
    }

}
