import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class ChapterCoverUrl1623178211094 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('chapter', new TableColumn({
            name: 'cover_url',
            type: 'varchar',
            isNullable: true
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('chapter', 'cover_url');
    }

}
