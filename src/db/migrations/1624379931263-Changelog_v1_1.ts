import {MigrationInterface, QueryRunner} from "typeorm";
import {Changelog} from "../entities/Changelog";

export class ChangelogV111624379931263 implements MigrationInterface {

    private version = "1.1.0";

    public async up(queryRunner: QueryRunner): Promise<void> {
        await Changelog.create({
            version: this.version,
            is_sent: false,
            text: `- Добавлена возможность скачать полностью вышедшую мангу одной книгой.\nФайл выъодит увесистый, потому не каждая читалка сможет его осилить.
            \n- Переработан генератор книг, теперь он работает чутка быстрее =)
            \n- Тайтлы, у которых еще не вышло ни одной главы (но они находятся через поиск) теперь лишены возможности просматривать список глав/томов а так же скачивать такую мангу, чтоб никого не смущать =)
            \n- За процессом генерации главы/целой манги теперь можно наблюдать. В процессе отображается номер главы/тома, который сейчас скачивается.
            \n- Еще небольшая пачка улучшений =)`
        }).save();
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await Changelog.delete({version: this.version});
    }

}
