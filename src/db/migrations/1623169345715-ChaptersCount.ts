import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class ChaptersCount1623169345715 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('manga', new TableColumn({
            name: 'chapters_count',
            type: 'int',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('manga', 'chapters_count')
    }

}
