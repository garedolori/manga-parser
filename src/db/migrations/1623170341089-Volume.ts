import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class Volume1623170341089 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'volume',
            columns: [
                {
                    name: 'id',
                    type: 'int(11) unsigned auto_increment',
                    isPrimary: true
                },
                {
                    name: 'manga_id',
                    type: 'int(11) unsigned'
                },
                {
                    name: 'name',
                    type: 'varchar'
                }
            ]
        }));

        await queryRunner.createForeignKey('volume', new TableForeignKey({
            columnNames: ['manga_id'], onDelete: "cascade", referencedColumnNames: ['id'], referencedTableName: "manga"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('volume')
    }

}
