import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class MenuMessageId1623152317966 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('user', new TableColumn({
            name: 'menu_message_id',
            type: 'int(11) unsigned',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('user', 'menu_message_id');
    }

}
