import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class VolumeFileUrl1623170946900 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('volume', new TableColumn({
            name: 'file_url',
            type: 'varchar',
            isNullable: true
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('volume', 'file_url');
    }

}
