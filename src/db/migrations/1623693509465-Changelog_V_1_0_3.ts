import {MigrationInterface, QueryRunner} from "typeorm";
import {Changelog} from "../entities/Changelog";

export class ChangelogV1031623693509465 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await Changelog.create({
            version: '1.0.3',
            is_sent: false,
            text: `- Добавлена возможность сортировать главы/томы манги в списке. По умолчанию показываются самы новые
            \n- Добавлена ссылка на последнюю вышедшую главу для онгоинг тайтов
            \n- Добавлена кнопка "Повторный поиск" на страницу результатов поиска
            \n- Небольшие исправения и улучшения`
        }).save()
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await Changelog.delete({version: '1.0.3'});
    }

}
