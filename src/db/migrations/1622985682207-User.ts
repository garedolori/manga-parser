import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class User1622985682207 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'user',
            columns: [
                {
                    name: 'id',
                    type: 'int(11) unsigned auto_increment',
                    isPrimary: true
                },
                {
                    name: 'name',
                    type: 'varchar'
                },
                {
                    name: 'key',
                    type: 'varchar'
                },
                {
                    name: 'is_premium',
                    type: 'int(1)'
                },
                {
                    name: 'created_at',
                    type: 'datetime',
                    default: 'NOW()'
                }
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('user');
    }

}
