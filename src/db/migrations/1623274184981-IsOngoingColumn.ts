import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class IsOngoingColumn1623274184981 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('manga', new TableColumn({
            name: 'is_ongoing',
            type: 'boolean',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('manga', 'is_ongoing')
    }

}
