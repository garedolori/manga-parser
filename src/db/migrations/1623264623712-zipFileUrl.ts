import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class zipFileUrl1623264623712 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('chapter', new TableColumn({
            name: 'zip_file_url',
            type: 'varchar',
            isNullable: true
        }));

        await queryRunner.addColumn('volume', new TableColumn({
            name: 'zip_file_url',
            type: 'varchar',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('chapter', 'zip_file_url');
        await queryRunner.dropColumn('volume', 'zip_file_url');
    }

}
