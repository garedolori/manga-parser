import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class Subscription1622986376055 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'subscription',
            columns: [
                {
                    name: 'id',
                    type: 'int(11) unsigned auto_increment',
                    isPrimary: true
                },
                {
                    name: 'user_id',
                    type: 'int(11) unsigned',
                },
                {
                    name: 'manga_id',
                    type: 'int(11) unsigned'
                }
            ]
        }));

        await queryRunner.createForeignKey('subscription', new TableForeignKey({
            columnNames: ['user_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'user',
            onDelete: 'cascade'
        }));

        await queryRunner.createForeignKey('subscription', new TableForeignKey({
            columnNames: ['manga_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'manga',
            onDelete: 'cascade'
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('subscription');
    }

}
