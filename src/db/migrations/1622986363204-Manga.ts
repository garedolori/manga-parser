import {MigrationInterface, QueryRunner, Table} from "typeorm";
import {addAbortSignal} from "stream";

export class Manga1622986363204 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'manga',
            columns: [
                {
                    name: 'id',
                    type: 'int(11) unsigned auto_increment',
                    isPrimary: true
                },
                {
                    name: 'name',
                    type: 'varchar'
                },
                {
                    name: 'created_at',
                    type: 'datetime',
                    default: 'NOW()'
                },
                {
                    name: 'root_url',
                    type: 'varchar'
                }
            ]
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('manga');
    }

}
