import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class MangaFileUrls1624050689736 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('manga', new TableColumn({
            name: 'file_url',
            type: 'varchar',
            isNullable: true
        }));

        await queryRunner.addColumn('manga', new TableColumn({
            name: 'zip_file_url',
            type: 'varchar',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('manga', 'file_url');
        await queryRunner.dropColumn('manga', 'zip_file_url');
    }

}
