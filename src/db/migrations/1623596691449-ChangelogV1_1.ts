import {MigrationInterface, QueryRunner} from "typeorm";
import {Changelog} from "../entities/Changelog";

export class ChangelogV111623596691449 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await Changelog.create({
            is_sent: false,
            text: `Добавлена возможность добавлсять мангу в закладки. Закладки теперь отображаются в главном меню.\nВ закладки можно добавить не только онгинг но и полностью вышедший тайтл.`,
            version: '1.0.1'
        }).save();
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await Changelog.delete({version: '1.0.1'});
    }
}
