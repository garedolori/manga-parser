import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Manga} from "./Manga";
import {Chapter} from "./Chapter";
import {File} from "./File";

@Entity()
export class Volume extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    manga_id: number;

    @Column()
    num?: number;

    @ManyToOne(type => Manga, manga => manga.volumes)
    @JoinColumn({name: 'manga_id'})
    manga: Manga;

    @OneToMany(() => Chapter, chapter => chapter.volume)
    @JoinColumn({name: 'volume_id'})
    chapters: Chapter[];

    private files: File[];

    public async getFiles(type: GeneratorFileType): Promise<File[]> {
        if (!this.files || !this.files.length) {
            this.files = await File.find({
                where: {
                    essence: "volume",
                    essence_id: this.id,
                    type
                }
            });
        }

        return this.files;
    }

    public async setFiles(files: string[], type: GeneratorFileType): Promise<File[]> {
        this.files = [];

        this.files = File.create(
            files.map(file_id => ({
                    essence: "volume",
                    essence_id: this.id,
                    file_id,
                    type
                })
            )
        )

        await Promise.all(this.files.map(file => file.save({reload: true})));

        return this.files;
    }
}
