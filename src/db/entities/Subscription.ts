import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";
import {Manga} from "./Manga";

@Entity()
export class Subscription extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    user_id: number

    @Column()
    manga_id: number

    @OneToOne(() => User)
    @JoinColumn({name: 'user_id'})
    user: User

    @OneToOne(() => Manga)
    @JoinColumn({name: 'manga_id'})
    manga: Manga
}
