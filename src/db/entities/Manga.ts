import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    JoinTable,
    ManyToMany,
    OneToMany,
    PrimaryGeneratedColumn
} from "typeorm";
import {User} from "./User";
import {Volume} from "./Volume";
import {Chapter} from "./Chapter";
import {Bookmark} from "./Bookmark";
import {File} from "./File";

@Entity()
export class Manga extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    created_at: Date;

    @Column()
    root_url: string;

    @Column()
    chapters_count?: number;

    @Column()
    is_ongoing?: boolean;

    @ManyToMany(() => User)
    @JoinTable({
        name: 'subscription',
        joinColumn: {
            name: 'manga_id',
            referencedColumnName: 'id'
        },
        inverseJoinColumn: {
            name: 'user_id',
            referencedColumnName: 'id'
        }
    })
    users: User[];

    @OneToMany(() => Volume, volume => volume.manga)
    @JoinColumn({name: 'manga_id'})
    volumes: Volume[];

    @OneToMany(() => Chapter, chapter => chapter.manga)
    @JoinColumn({name: 'manga_id'})
    chapters: Chapter[];

    @ManyToMany(type => User)
    @JoinTable({
        name: 'bookmark',
        joinColumn: {
            name: 'manga_id',
            referencedColumnName: 'id'
        },
        inverseJoinColumn: {
            name: 'user_id',
            referencedColumnName: 'id'
        }
    })
    bookmarkUsers: User[];

    private files: File[];

    public async getFiles(type: GeneratorFileType): Promise<File[]> {
        if (!this.files || !this.files.length) {
            this.files = await File.find({
                where: {
                    essence: "manga",
                    essence_id: this.id,
                    type
                }
            });
        }

        return this.files;
    }

    public async setFiles(files: string[], type: GeneratorFileType): Promise<File[]> {
        this.files = [];

        this.files = File.create(
            files.map(file_id => ({
                    essence: "manga",
                    essence_id: this.id,
                    file_id,
                    type: type
                })
            )
        )

        await Promise.all(this.files.map(file => file.save({reload: true})));

        return this.files;
    }
}
