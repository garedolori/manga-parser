import {BaseEntity, Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Manga} from "./Manga";
import {Subscription} from "./Subscription";
import {Bookmark} from "./Bookmark";

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    key: string;

    @Column()
    is_premium: boolean;

    @Column()
    created_at: Date;

    @Column()
    menu_message_id?: number;

    @ManyToMany(() => Manga)
    @JoinTable({
        name: 'subscription',
        joinColumn: {
            name: 'user_id',
            referencedColumnName: 'id'
        },
        inverseJoinColumn: {
            name: 'manga_id',
            referencedColumnName: 'id'
        }
    })
    manga: Manga[];

    @ManyToMany(() => Manga)
    @JoinTable({
        name: 'bookmark',
        joinColumn: {
            name: 'user_id',
            referencedColumnName: 'id'
        },
        inverseJoinColumn: {
            name: 'manga_id',
            referencedColumnName: 'id'
        }
    })
    bookmarks: Manga[];

    public async subscribe(manga_id:number):Promise<void> {
        const existingSub = await Subscription.findOne({where: {user_id: this.id, manga_id}});

        if (!existingSub) {
            await Subscription.create({
                manga_id,
                user_id: this.id
            }).save();
        }
    }

    public async unsubscribe(manga_id:number):Promise<void> {
        await Subscription.delete({user_id: this.id, manga_id});
    }

    public async addToBookmarks(manga_id:number):Promise<void> {
        const existingBookmark = await Bookmark.findOne({where: {user_id: this.id, manga_id}});

        if (!existingBookmark) {
            await Bookmark.create({
                manga_id,
                user_id: this.id
            }).save();
        }
    }

    public async removeBookmark(manga_id:number):Promise<void> {
        await Bookmark.delete({user_id: this.id, manga_id});
    }
}
