import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class File extends BaseEntity {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    essence:"volume"|"chapter"|"manga";

    @Column()
    essence_id:number;

    @Column()
    type:GeneratorFileType|"zip"; //zip is legacy

    @Column()
    file_id:string;
}
