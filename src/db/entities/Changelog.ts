import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Changelog extends BaseEntity {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    is_sent:boolean;

    @Column()
    text:string;

    @Column()
    created:Date;

    @Column()
    version:string
}
