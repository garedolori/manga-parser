import {BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Manga} from "./Manga";
import {Volume} from "./Volume";
import {File} from "./File";

@Entity()
export class Chapter extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    manga_id: number;

    @Column()
    name: string;

    @Column()
    number: number;

    @Column()
    created_at: Date;

    @Column()
    volume_id?: number;

    @Column()
    root_url: string;

    @Column()
    cover_url?: string;

    @ManyToOne(() => Manga, manga => manga.chapters)
    @JoinColumn({name: 'manga_id'})
    manga: Manga;

    @ManyToOne(() => Volume, volume => volume.chapters)
    @JoinColumn({name: 'volume_id'})
    volume: Volume;

    private files: File[];

    public async getFiles(type: GeneratorFileType): Promise<File[]> {
        if (!this.files || !this.files.length) {
            this.files = await File.find({
                where: {
                    essence: "chapter",
                    essence_id: this.id,
                    type
                }
            });
        }

        return this.files;
    }

    public async setFiles(files: string[], type: GeneratorFileType): Promise<File[]> {
        this.files = [];

        this.files = File.create(
            files.map(file_id => ({
                    essence: "chapter",
                    essence_id: this.id,
                    file_id,
                    type: type
                })
            )
        )

        await Promise.all(this.files.map(file => file.save({reload: true})));

        return this.files;
    }
}
