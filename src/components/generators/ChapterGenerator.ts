import FileGenerator from "./abstract/FileGenerator";
import {FileGeneratorInterface} from "./abstract/FileGeneratorInterface";
import {Chapter} from "../../db/entities/Chapter";

export default class ChapterGenerator extends FileGenerator implements FileGeneratorInterface {

    constructor(private chapter:Chapter, private images:string[], protected fileType:GeneratorFileType) {
        super();
        this.series = this.chapter.manga.name;

        let number = `${chapter.number}`;
        for (let i = number.length; i < 4; i++) number = `0${number}`;

        this.htmlFileName = `${number} ${chapter.manga.name} ${chapter.name.replace(chapter.volume.name, '')}.html`;
        this.title = `${chapter.manga.name}: ${chapter.name.replace(chapter.volume.name, '')}`;
    }

    async getFile(): Promise<string[]> {
        super.prepare();

        const {chapter, images} = this;

        console.log(`[CHAPTER GENERATOR] generating chapter ${chapter.name}`)
        const tags = await this.getImagesTags(images);

        this.contentParts = images.map(image => tags[image]);
        console.log(`[VOLUME GENERATOR] done`);

        this.files.push(await this.getFilePath());

        return this.upload();
    }
}