import FileGenerator from "./abstract/FileGenerator";
import {FileGeneratorInterface} from "./abstract/FileGeneratorInterface";
import {Volume} from "../../db/entities/Volume";
import fs from "fs";

export default class VolumeGenerator extends FileGenerator implements FileGeneratorInterface {

    constructor(private volume:Volume, private volumeImages:ChaptersImagesMap, protected fileType:GeneratorFileType) {
        super();
        this.series = this.volume.manga.name;

        let number = `${volume.num}`;
        for (let i = number.length; i < 3; i++) number = `0${number}`;

        this.htmlFileName = `${number} ${volume.manga.name} ${volume.name}.html`
        this.title = `${volume.manga.name}: ${volume.name}`;
    }

    async getFile(): Promise<string[]> {
        this.isProcessRunning = true;
        super.prepare();

        const {volume, volumeImages} = this;

        const chaptersOrder = volume.chapters
            .sort((a, b) => a.number < b.number ? -1 : 1)

        let i = 0
        const fileSizeLimit = 1500 * 1024 * 1024 // 1.5 Gb

        for (let chapter of chaptersOrder) {
            console.log(`[VOLUME GENERATOR] generating ${++i} of ${chaptersOrder.length}`);
            this.emit("chapterDownload", {current: i, total: chaptersOrder.length});

            const images = volumeImages[chapter.id];

            await fs.promises.mkdir(`${this.imagesDir}/${chapter.id}`)

            const tags = await this.getImagesTags(images, `${chapter.id}`);

            this.contentParts.push(`<div id="chapter-${chapter.number}">${images.map(image => tags[image]).join(`\n`)}</div>`);
            this.addToToc(`#chapter-${chapter.number}`, chapter.name.replace(volume.name, ''));

            if (this.bytesDownloaded >= fileSizeLimit) {
                console.log("[MANGA GENERATOR] filesize exceeded 1.5 Gb. Saving file to fs and generating new.")
                this.emit("filesizeExceeded", this.partNumber +1);

                this.files.push(await this.getFilePath());

                this.partNumber++;
                this.bytesDownloaded = 0;

                await this.prepare();
            }

            console.log(`[VOLUME GENERATOR] done`);
        }

        this.files.push(await this.getFilePath());

        return this.upload();
    }


}