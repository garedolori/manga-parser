import FileGenerator from "./abstract/FileGenerator";
import {FileGeneratorInterface} from "./abstract/FileGeneratorInterface";
import {Manga} from "../../db/entities/Manga";
import * as fs from "fs";

export default class MangaGenerator extends FileGenerator implements FileGeneratorInterface {

    constructor(private manga: Manga, private volumeImages: VolumesChaptersImagesMap, protected fileType:GeneratorFileType) {
        super();
        this.series = manga.name;
        this.htmlFileName = `${manga.name}.html`;
        this.title = manga.name;
    }

    async getFile(): Promise<string[]> {
        this.isProcessRunning = true;
        super.prepare();

        const {manga, volumeImages} = this;

        const volumesOrder = manga.volumes.sort((a, b) => a.num < b.num ? -1 : 1);

        let part = 0;
        const fileSizeLimit = 1500 * 1024 * 1024 // 1.5 Gb

        for (let volume of volumesOrder) {
            console.log(`[MANGA GENERATOR] generating ${volume.num} of ${volumesOrder.length}`);
            this.emit("volumeDownload", {current: volume.num, total: volumesOrder.length});

            this.addToToc(`#volume-${volume.num}`, volume.name);

            const sortedChapters = volume.chapters
                .sort((a, b) => a.number < b.number ? -1 : 1);

            const chaptersContent = [];

            for (let chapter of sortedChapters) {
                const images = volumeImages[volume.id][chapter.id]

                await fs.promises.mkdir(`${this.imagesDir}/${chapter.id}`)

                const tags = await this.getImagesTags(images, `${chapter.id}`);

                chaptersContent.push(`<div id="chapter-${chapter.number}">${images.map(image => tags[image]).join(`\n`)}</div>`);
                this.addToToc(`#chapter-${chapter.number}`, chapter.name.replace(volume.name, ''));

                await new Promise(resolve => setTimeout(resolve, 1000));

                console.log(this.bytesDownloaded);

                if (this.bytesDownloaded >= fileSizeLimit) {
                    console.log("[MANGA GENERATOR] filesize exceeded 1.5 Gb. Saving file to fs and generating new.")
                    this.emit("filesizeExceeded", this.partNumber);

                    this.contentParts.push(`<div id="volume-${volume.num}">${chaptersContent.join('\n')}</div>`)
                    this.files.push(await this.getFilePath());

                    this.partNumber++;
                    this.bytesDownloaded = 0;

                    await this.prepare();
                    this.addToToc(`#volume-${volume.num}`, `${volume.name} (Продолжение)`);
                }
            }

            this.contentParts.push(`<div id="volume-${volume.num}">${chaptersContent.join('\n')}</div>`)

            console.log(`[MANGA GENERATOR] Done`);
        }

        this.emit("downloadComplete", true);

        this.files.push(await this.getFilePath());

        return this.upload();
    }
}