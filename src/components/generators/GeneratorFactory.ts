import {FileGeneratorInterface, VolumeWithChapters} from "./abstract/FileGeneratorInterface";
import {Chapter} from "../../db/entities/Chapter";
import {Manga} from "../../db/entities/Manga";
import ChapterGenerator from "./ChapterGenerator";
import VolumeGenerator from "./VolumeGenerator";
import MangaGenerator from "./MangaGenerator";

export default abstract class GeneratorFactory {
    private static runningGenerators = {
        chapter: {
            mobi: {},
            epub: {}
        },
        volume: {
            mobi: {},
            epub: {}
        },
        manga: {
            mobi: {},
            epub: {}
        }
    }

    public static getChapterGenerator( chapter: Chapter, images: string[], type:GeneratorFileType): FileGeneratorInterface {
        if (!this.runningGenerators.chapter[type][chapter.id]) {
            const generator = new ChapterGenerator(chapter, images, type);

            generator.on("finish", () => {
                delete this.runningGenerators.chapter[type][chapter.id];
            })

            this.runningGenerators.chapter[type][chapter.id] = generator;
        }

        return this.runningGenerators.chapter[type][chapter.id];
    }

    public static getVolumeGenerator(volume: VolumeWithChapters, chapterImages: ChaptersImagesMap, type:GeneratorFileType): FileGeneratorInterface {
        if (!this.runningGenerators.volume[type][volume.id]) {
            const generator = new VolumeGenerator(volume, chapterImages, type);

            generator.on("finish", () => {
                delete this.runningGenerators.volume[type][volume.id];
            })

            this.runningGenerators.volume[type][volume.id] = generator;
        }

        return this.runningGenerators.volume[type][volume.id];
    }

    public static getMangaGenerator(manga: Manga, volumesList: VolumesChaptersImagesMap, type:GeneratorFileType): FileGeneratorInterface {
        if (!this.runningGenerators.manga[type][manga.id]) {
            const generator = new MangaGenerator(manga, volumesList, type);

            generator.on("finish", () => {
                delete this.runningGenerators.manga[type][manga.id];
            })

            this.runningGenerators.manga[type][manga.id] = generator;
        }

        return this.runningGenerators.manga[type][manga.id];
    }
}