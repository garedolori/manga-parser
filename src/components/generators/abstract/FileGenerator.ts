import * as fs from "fs";
import axios from "axios";
import EventEmitter from "events";
import App from "../../../App";
import * as child_process from "child_process";
import {spawn} from "child_process";

export default abstract class FileGenerator {
    protected fileName: string;
    protected template: string = `<html><head><title>{title}</title><meta name="author" content="https://t.me/MangaLibParserBot"/></head><body>
            <div id="body">{body}</div><div id="toc">{toc}</div>
        </body></html>`;

    protected title: string;
    protected emitter: EventEmitter;

    protected bytesDownloaded: number = 0;

    protected rootDir: string;
    protected imagesDir: string;
    protected htmlFileName: string;

    protected tocParts: string[] = [];
    protected contentParts: string[] = [];

    protected fileType: GeneratorFileType;

    protected partNumber: number = 0;

    public isProcessRunning: boolean = false;

    protected files?: string[] = [];
    protected workdirs: string[] = [];

    protected series: string;

    protected constructor() {
        this.emitter = new EventEmitter();
        this.rootDir = `/tmp/${Date.now()}`;
        this.imagesDir = `${this.rootDir}/img`;
    }

    public async removeFile(): Promise<void> {
        for (let dir of this.workdirs) {
            await fs.promises.rmdir(dir, {recursive: true});
        }
    }

    public on(event: string, handler: (...args: any[]) => void) {
        return this.emitter.on(event, handler);
    }

    protected emit(event: string, data: any) {
        return this.emitter.emit(event, data);
    }

    protected prepare(): void {
        if (this.partNumber) {
            this.rootDir = `${this.rootDir}-${this.partNumber}`;
            this.imagesDir = `${this.rootDir}/img`;
        }

        fs.mkdirSync(this.rootDir);
        fs.mkdirSync(this.imagesDir);

        this.workdirs.push(this.rootDir);
    }

    protected async getFilePath(): Promise<string> {
        const content = this.template
            .replace('{title}', `${this.title} ${this.partNumber || ''}`)
            .replace('{body}', this.contentParts.join(''))
            .replace('{toc}', this.tocParts.join(''));

        const fileName = this.partNumber
            ? this.htmlFileName.replace('.html', `-${this.partNumber}.html`)
            : this.htmlFileName;

        const filePath = `${this.rootDir}/${fileName}`;

        await fs.promises.writeFile(filePath, content);

        this.contentParts = [];
        this.tocParts = [];

        return filePath;
    }


    protected async getImagesTags(images: string[], appendPath?: string): Promise<{ [name: string]: string }> {
        const imageFileTag: { [name: string]: string } = {};

        const imagesFilenameLength = images.length.toString().length;

        await Promise.all(images.map((image, index, {length}) => {
            return axios({
                method: "get",
                url: image,
                responseType: "arraybuffer"
            }).then(({data}) => {
                this.bytesDownloaded += data.length;
                let imageName = index.toString();

                if (imageName.length !== imagesFilenameLength) {
                    for (let i = imageName.length; imageName.length < imagesFilenameLength; i++) {
                        imageName = `0${imageName}`;
                    }
                }

                const url = `${this.imagesDir}${appendPath ? `/${appendPath}` : ''}/${imageName}.${image.split('.').reverse()[0]}`;

                imageFileTag[image] = `<div><img src="${url}" width="100%"></div>`;

                return fs.promises.writeFile(url, data);
            }).catch((e) => Promise.reject(e))
        }));

        return imageFileTag;
    }

    async upload(): Promise<string[]> {
        const result = [];

        for (let file of this.files) {
            const targetFile = file.replace('html', this.fileType);

            await new Promise((resolve, reject) => {

                const args = [file, targetFile, '--output-profile=tablet'];
                if (this.series) args.push(`--series=${this.series}`);

                const childProcess = spawn(`ebook-convert`, args);

                childProcess.on("close", code => {
                    if (code === 0) {
                        childProcess.stdin.end();
                        return resolve(code);
                    }

                    return reject(`Calibre process exit with code ${code}`);
                });

                childProcess.stdout.on("data", data => console.log(data.toString()));
                childProcess.stderr.on("data", data => console.error(data.toString()));
            })

            result.push(await App.getInstance().uploader.uploadLocalFile(targetFile));

            await new Promise(resolve => setTimeout(resolve, 5000));
        }

        this.emit("finish", result);

        await this.removeFile();

        return result;
    }

    protected addToToc(link, title) {
        this.tocParts.push(`<span> <a href="${link}">${title}</a></span>`)
    }
}