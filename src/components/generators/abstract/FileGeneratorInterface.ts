import {Chapter} from "../../../db/entities/Chapter";
import {Volume} from "../../../db/entities/Volume";

export interface VolumeWithChapters extends Volume {
    chapters:Chapter[]
}

export interface FileGeneratorInterface {
    getFile():Promise<string[]>
    removeFile():Promise<void>
    on(event:string, handler:(...args:any[]) => any):void
    isProcessRunning:boolean;
}