import axios from "axios";
import {JSDOM, Node} from 'jsdom';
import {Manga} from "../db/entities/Manga";
import {Chapter} from "../db/entities/Chapter";
import {
    FileGeneratorInterface,
    VolumeWithChapters
} from "./generators/abstract/FileGeneratorInterface";
import GeneratorFactory from "./generators/GeneratorFactory";

export default class MangaRemote {
    private static baseUrl = 'https://mangapoisk.ru';
    private ongoingDetectorString = "Выпускается";
    private statusDetectorString =  "Статус:"

    public name: string;
    public coverUrl: string;
    public chapters: number;
    public description:string;
    public isOngoing:boolean;

    private document:JSDOM.Node;
    private chapterList:Array<any> = [];
    private volumeList:Array<any> = [];

    public static async search(query):Promise<Manga[]> {
        const {data} = await axios.get(`${this.baseUrl}/search?q=${encodeURIComponent(query)}`);

        return this.fromSearchResults(data.results);
    }

    private static async fromSearchResults(searchResults: MangaRemoteSearchResult[]): Promise<Manga[]> {
        const mangaCol = [];
        for (let result of searchResults) {
            const existingManga = await Manga.findOne({where: {root_url: result.link}});
            if (existingManga) {
                mangaCol.push(existingManga);
                continue;
            }

            const manga = new Manga();
            manga.name = result.label;
            manga.root_url = result.link;

            await manga.save();

            mangaCol.push(manga);
        }

        return mangaCol;
    }

    public static async fromUrl(url:string) {
        const instance = new this(url);
        return instance.build();
    }

    public static async getChapterGenerator(chapter_id:number, type:GeneratorFileType):Promise<FileGeneratorInterface> {
        const chapter = await Chapter.findOne({where: {id: chapter_id}, relations: ['manga', 'volume']});

        const images = await this.getChapterImages(chapter.root_url);

        if (!chapter.cover_url) {
            chapter.cover_url = images[0];
            await chapter.save({reload: true});
        }

        return GeneratorFactory.getChapterGenerator(chapter, images, type);
    }

    public static async getVolumeGenerator(volume:VolumeWithChapters, type:GeneratorFileType):Promise<FileGeneratorInterface> {
        const chapterImages = {};

        for (let chapter of volume.chapters) {
            chapterImages[chapter.id] = await this.getChapterImages(chapter.root_url);
        }

        return GeneratorFactory.getVolumeGenerator(volume, chapterImages, type);
    }

    public static async geMangaGenerator(manga_id:number, type:GeneratorFileType): Promise<FileGeneratorInterface> {
        const manga = await Manga.findOne({
            where: {id: manga_id},
            relations: ['volumes', 'volumes.chapters']
        });

        const volumesList = {};

        for (let volume of manga.volumes) {
            volumesList[volume.id] = {};

            await Promise.all(volume.chapters.map(chapter => {
                return this.getChapterImages(chapter.root_url)
                    .then(images => volumesList[volume.id][chapter.id] = images);
            }));
        }

        return GeneratorFactory.getMangaGenerator(manga, volumesList, type);
    }

    public static async getChapterImages(chapterUrl:string):Promise<string[]> {
        const {data:page} = await axios.get(`${this.baseUrl}${chapterUrl}`);

        const {window: {document}} = new JSDOM(page);

        const imageNodes = document.querySelectorAll('img.page-image');

        return Array.from(imageNodes).map(imageNode => imageNode['dataset'].src || imageNode["src"])
    }

    protected constructor(public url:string) {
    }

    public async build(): Promise<MangaRemote>{
        const {data:page} = await axios.get(`${MangaRemote.baseUrl}${this.url}`);

        const {window: {document}} = new JSDOM(page);

        this.document = document;

        const infoBlock = this.document.querySelector('article');

        this.name = infoBlock.querySelector('.entry-title').textContent.trim().replace(/\n/g, ' ').replace(/\s{2,}/g, ' ');
        this.coverUrl = infoBlock.querySelector('img').src;

        const descriptionBlock = infoBlock.querySelector('.manga-description>div');
        this.description = descriptionBlock ? descriptionBlock.textContent.trim(): '';

        const infoParts = infoBlock.querySelectorAll('span b');

        const ongoingDetectionNode = Array.from(infoParts).find( (node:Node) => node.textContent.trim() === this.statusDetectorString);
        this.isOngoing = ongoingDetectionNode
            ? ongoingDetectionNode["nextSibling"].textContent.trim() === this.ongoingDetectorString
            : true

        const chapterList = document.querySelector('.chaptersList');
        const chaptersText = chapterList.querySelector('h2.h5').textContent.trim();

        this.chapters = parseInt(chaptersText.match(/\((\d+)\)/)[1]) || 0;

        return this;
    }

    public async getChapterList() {
        if (this.chapterList.length) {
            return this.chapterList;
        }

        const chapters = [];
        const chapterList = this.document.querySelector('.chapter-list-container');

        const nav = this.document.querySelector('.chapters-infinite-pagination>nav');

        if (nav) {
            let nextPageLink = nav.querySelector('a.page-link[rel="next"]')

            while (nextPageLink) {
                const url = nextPageLink.href;
                const {data: content} = await axios.get(`${MangaRemote.baseUrl}${url}`);
                const {window: {document: elements}} = new JSDOM(content);

                const children = elements.querySelectorAll('li.chapter-item');

                for (let child of children) {
                    chapterList.appendChild(child);
                }

                nextPageLink = elements.querySelector('a.page-link[rel="next"]');
            }
        }

        const chapterItems = chapterList.querySelectorAll('a');

        for (let item of chapterItems) {
            const name = item.textContent.trim().replace(/\n/g, ' ').replace(/\s{2,}/g, ' ');
            const volumeMatch = name.match(/(Том \d+)/);

            chapters.push({
                url: item.href,
                name,
                volume: volumeMatch ? volumeMatch[0] : 'TOM 1'
            });
        }

        this.chapterList = chapters;

        return this.chapterList;
    }

    public async getVolumeList() {
        const chapters = await this.getChapterList();

        const volumesMap = {};

        let volumeNum = 1;
        for (let chapter of chapters.reverse()) {
            if (!volumesMap[chapter.volume]) {
                volumesMap[chapter.volume] = {
                    num: volumeNum++,
                    name: chapter.volume,
                    chapters: []
                }
            }

            volumesMap[chapter.volume].chapters.push(chapter);
        }

        this.volumeList = Object.values(volumesMap)

        return this.volumeList;
    }
}