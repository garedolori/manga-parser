import {EventEmitter} from "events";
import Client from "tdl";

export default class TgFileUploader extends EventEmitter {
    constructor(private client:Client) {
        super();
        client.on("update", this.updateListener.bind(this))
    }

    private updateListener(update) {
        if (update._ === 'updateFile') {
            const {id, remote} = update.file;
            if (remote && remote.is_uploading_completed && remote.id) {
                this.emit('uploadFinished', {id, file_id: remote.id});
            }
        }
    }

    public async uploadLocalFile(path:string):Promise<string> {
        try {
            const chat_id = parseInt(process.env.BOT_CHAT_ID);
            const message = await this.client.invoke({
                _:"sendMessage",
                chat_id,
                input_message_content: {
                    _:"inputMessageDocument",
                    document: {
                        _:"inputFileLocal",
                        path
                    }
                }
            });

            return new Promise((resolve, reject) => {
                this.on("uploadFinished", e => {
                    if (e.id === message.content['document'].document.id) {
                        resolve(e.file_id);
                        this.client.invoke({
                            _:"deleteMessages",
                            message_ids: [message.id],
                            chat_id
                        })
                            .then(() => resolve(e.file_id))
                            .catch(reject)
                    }
                })
            });
        } catch (e) {
            console.log(e);
            throw Error(e.message || JSON.stringify(e));
        }
    }

}