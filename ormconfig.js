module.exports = {
    type: "mysql",
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT || 3306,
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DB,
    migrations: ["src/db/migrations/*.ts"],
    entities: ["src/db/entities/*.ts"],
    cli: {
        migrationsDir: './src/db/migrations',
        entitiesDir: './src/db/entities'
    },
    logging: "error"
}